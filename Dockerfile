#Deploy prometheus / node bundle w/ promdash
from ubuntu:14.04
maintainer joshua@joshuaharshman.com

#Environment variables
env SRVPATH "/Prometheus/server"
env PROBEPATH "/Prometheus/probe"
env DBPATH "/Prometheus/databases"
env RAILS_ENV "production"
env DATABASE_URL "sqlite3:/Prometheus/databases/mydb.sqlite3"
env BUNDLE_GEMFILE "/Prometheus/promdash/Gemfile"

run apt-get update && \
    apt-get install -y \ 
        wget \
        ruby \
        git \
        bundler \
        libsqlite3-dev \
        sqlite3 zlib1g-dev

run mkdir -p "${SRVPATH}" \
    && wget https://github.com/prometheus/prometheus/releases/download/0.15.1/prometheus-0.15.1.linux-amd64.tar.gz -O "${SRVPATH}"/prometheus.tar.gz \
    && tar zxvf "${SRVPATH}"/prometheus.tar.gz --directory "${SRVPATH}"

copy prometheus.yml "${SRVPATH}"/

run mkdir -p "${PROBEPATH}" \
    && wget https://github.com/prometheus/node_exporter/releases/download/0.11.0/node_exporter-0.11.0.linux-amd64.tar.gz -O "${PROBEPATH}"/probe.tar.gz \
    && tar zxvf "${PROBEPATH}"/probe.tar.gz --directory "${PROBEPATH}" \
    && ln -s "${PROBEPATH}"/node_exporter /usr/bin

workdir "Prometheus/"
run git clone https://github.com/prometheus/promdash.git 
workdir "promdash/"
run bundle install --without mysql postgresql

run mkdir -p "${DBPATH}" \
    && rake db:migrate \
    && rake assets:precompile

expose 9100 9090 3000

workdir "/"
copy companion.sh ./
run chmod +x companion.sh
entrypoint /companion.sh
