# README #

### Summary ###

Dockerfile and companion script for deploying a full prometheus
environment on a single node

## Includes ##

* Prometheus Server
* Node Exporter
* PromDash

### How do I get set up? ###

* If Docker is not installed on your system
`apt-get install docker`
`groupadd docker`
`gpasswd -a <user> docker`

* Build the image
`docker build -t prometheus-demo .`

* Run the container
`docker run --name prometheus-full -d -p 3000:3000 -p 9090:9090 -p 9100:9100 prometheus-demo`

