#!/bin/bash
nohup node_exporter &
nohup Prometheus/server/prometheus --config.file=Prometheus/server/prometheus.yml > prometheus.log 2>&1 &
nohup bundle exec thin -c /Prometheus/promdash/ start
